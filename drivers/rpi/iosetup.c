#include <gpio.h>
#include <debug.h>

#include "uart.h"
#include "sdcard.h"

void iosetup()
{
    setup_gpio();

    if (setup_sdcard() < 0)
    {
        kprintf(KFATAL, "fatal error: could not initialize EMMC controller\n");
        panic();
    }
}
