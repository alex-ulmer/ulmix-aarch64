#ifndef RPI_H
#define RPI_H

// setup board-specific hardware
void iosetup();

unsigned long long rpi_serial();

#endif // RPI_H
