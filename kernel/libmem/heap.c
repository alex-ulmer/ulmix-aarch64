#include <heap.h>
#include <debug.h>
#include <asm.h>
#include <types.h>

struct kheape_struct *_heap_start = NULL;

void __init setup_heap(void *start_addr, unsigned long max_size)
{
    if (start_addr == 0x0)
    {
        /* don't start at 0x00, because that
         * value is used as the invalid pointer. */
        start_addr = (void*)0x01;
        max_size -= 1;
    }

    kprintf(KDEBUG, "setting up heap at %p (size %S)\n", start_addr, max_size);
    struct kheape_struct *first_entry = (struct kheape_struct*)start_addr;
    first_entry->available = 1;
    first_entry->description = NULL;
    first_entry->next = NULL;
    first_entry->previous = NULL;
    first_entry->size = max_size - sizeof(struct kheape_struct);
    first_entry->start = start_addr + sizeof(struct kheape_struct);

    _heap_start = first_entry;
}

void heap_dump(void)
{
    struct kheape_struct *entry;
    for (entry = _heap_start; entry != NULL; entry = entry->next)
    {
        kprintf(KDEBUG, "avlb=%s, start=%p, size=%S, \"%s\"\n",
              entry->available ? "yes" : "no ",
              entry->start,
              entry->size,
              entry->description);
    }
}
