# ulmix
Ulmix Operating System for RPi 3 (AArch64)

## Compiling

To successfully compile Ulmix, a crosscompiler is needed. Debian/Ubuntu provides those in package form:

```sh
sudo apt install binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu gcc-8-aarch-linux-gnu build-essential
```

You should then be able to build Ulmix with no problems using the makefile in the repo root.

```bash
make all
```

Directories:
* `libc/` kernel and userspace shared C library (arch independent)
* `libk/` kernel core functionality (arch independent)
* `arch/` architecture dependent code
* `arch/aarch64/` code for AArch64 (ARMv8), e.g. Raspberry Pi (assmbly, ...)
* `drivers/` board specific code (mostly drivers)
* `drivers/rpi3` drivers for Raspberry Pi (SoC BCM2835/36/37)
* `tools/` helper makefile, JTAG config, auxiliary tools

## Running on Hardware
On the Raspi3 SD card has to be a at least a FAT32 file system, from which the GPU bootloader will load the kernel. On that file system, at least these files have to be present:
* `bootcode.bin`, the GPU firmware
* `start.elf`, the actual bootloader
* `kernel8.img`, the actual kernel

The kernel can also be named `kernel.img` or `kernel7.img`, however, in this case they won't be executed in AArch64 mode but in AArch32 mode. Ergo, the 64 Bit kernel has to be named kernel8.img.

## Running with QEMU
Although QEMU now supports the 'raspi3' target platform, it is not yet contained in the official package manager repositories. It has to be built from source (June 2019).

After that, starting Ulmix looks like this:

```bash
qemu-system-aarch64 -M raspi3 -kernel kernel8.img -serial stdio
```

## Debugging with QEMU
Debugging with QEMU is very straightforward, just pass the additional arguments `-S -s` to it.

Then connect your `gdb-multiarch` to remote target `localhost:1234`. Probably, the package `gdb-multiarch` has to be previously installed`.

Symbols are contained in the `vmulmix` binary (ELF).
