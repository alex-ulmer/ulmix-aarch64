include tools/tools.mk

KERNEL 		= vmulmix
KERNIMG		= kernel8.img

LIBC		:= libc/libc.a
LIBK		:= kernel/libk.a
LIBTBOARD	:= drivers/$(TARGET_BOARD)/lib$(TARGET_BOARD).a
ARCHDEPS	:= arch/$(TARGET_ARCH)/lib$(TARGET_ARCH).a

LIBS		:= $(ARCHDEPS) $(LIBK) $(LIBTBOARD) $(LIBK) $(LIBC)

LDINFO		= arch/$(TARGET_ARCH).ld

all: $(KERNEL)

$(KERNEL): $(LIBS)
	@ echo " LD  $(KERNEL)"
	@ $(LD) $(LDFLAGS) $(LIBS) -o $(KERNEL)
	@ $(OBJCOPY) $(KERNEL) -O binary $(KERNIMG)

%.a:
	@ make --no-print-directory -C $(dir $@)

clean:
	rm -f $(KERNEL) $(KERNIMG)
	make clean -C kernel
	make clean -C arch/$(TARGET_ARCH)
	make clean -C libc
	make clean -C drivers/$(TARGET_BOARD)

.phony: all clean
