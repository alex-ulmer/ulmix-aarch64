#include <gpio.h>
#include <types.h>
#include <asm.h>

#define GPFSEL0         MMIO(0x00200000)
#define GPFSEL1         MMIO(0x00200004)
#define GPFSEL2         MMIO(0x00200008)
#define GPFSEL3         MMIO(0x0020000C)
#define GPFSEL4         MMIO(0x00200010)
#define GPFSEL5         MMIO(0x00200014)
#define GPSET0          MMIO(0x0020001C)
#define GPSET1          MMIO(0x00200020)
#define GPCLR0          MMIO(0x00200028)
#define GPCLR1          MMIO(0x0020002c)
#define GPLEV0          MMIO(0x00200034)
#define GPLEV1          MMIO(0x00200038)
#define GPEDS0          MMIO(0x00200040)
#define GPEDS1          MMIO(0x00200044)
#define GPREN0          MMIO(0x0020004c)
#define GPREN1          MMIO(0x00200050)
#define GPFEN0          MMIO(0x00200058)
#define GPFEN1          MMIO(0x0020005c)
#define GPHEN0          MMIO(0x00200064)
#define GPHEN1          MMIO(0x00200068)
#define GPLEN0          MMIO(0x00200070)
#define GPLEN1          MMIO(0x00200074)
#define GPPUD           MMIO(0x00200094)
#define GPPUDCLK0       MMIO(0x00200098)
#define GPPUDCLK1       MMIO(0x0020009C)

void gpio_setpull(int pin, unsigned rpull)
{
    volatile unsigned int *CLKREG;
    if (pin < 32)       CLKREG = GPPUDCLK0;
    else if (pin < 54)  CLKREG = GPPUDCLK1;

    int pinbit = (1 << (pin % 32));

    *GPPUD = rpull;
    wait_cycles(150);
    *CLKREG = pinbit;
    wait_cycles(150);
    *CLKREG = 0;
    *GPPUD = 0;
}

void gpio_pinmode(int pin, unsigned pinmode, unsigned rpull)
{
    volatile unsigned int *GPFREG;
    register unsigned int r;

    if (pin < 10)       GPFREG = GPFSEL0;
    else if (pin < 20)  GPFREG = GPFSEL1;
    else if (pin < 30)  GPFREG = GPFSEL2;
    else if (pin < 40)  GPFREG = GPFSEL3;
    else if (pin < 50)  GPFREG = GPFSEL4;
    else if (pin < 54)  GPFREG = GPFSEL5;
    else return;

    int gpf_index = pin % 10;
    pinmode &= 0x7;

    r = *GPFREG;
    r &= ~(0x7 << (gpf_index * 3));
    r |= (pinmode << (gpf_index * 3));
    *GPFREG = r;

    gpio_setpull(pin, rpull);
}

void gpio_set(int pin, int val)
{
    volatile unsigned int *GPSREG;

    if (pin < 32 && val)        GPSREG = GPSET0;
    else if (pin < 54 && val)   GPSREG = GPSET1;
    else if (pin < 32)          GPSREG = GPCLR0;
    else if (pin < 54)          GPSREG = GPCLR1;
    else return;

    int pin_index = pin % 32;
    *GPSREG |= (1 << pin_index);
}

void gpio_trigger_enable(int pin, unsigned trigmode)
{
    volatile unsigned int *ENREG;

    switch (trigmode)
    {
    case GPIO_EDGE_RISE: ENREG = GPREN0;
        break;
    case GPIO_EDGE_FALL: ENREG = GPFEN0;
        break;
    case GPIO_LVL_HIGH:  ENREG = GPHEN0;
        break;
    case GPIO_LVL_LOW:   ENREG = GPLEN0;
    }

    if (pin >= 32)
        ENREG++;

    *ENREG |= (1 << (pin % 32));
}

void setup_gpio(void)
{
    // configure RPi 3B+ ACT status led
    gpio_pinmode(GPIO_ACTLED, GPIO_OUTPUT, GPIO_NOPULL);
}
