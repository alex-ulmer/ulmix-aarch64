#include <board.h>
#include <types.h>
#include <mbox.h>

// hardcoded for now:

unsigned long _mmio_base = 0x3f000000;

unsigned long long rpi_serial()
{
    mbox[0] = 8*4;
    mbox[1] = MBOX_REQUEST;

    mbox[2] = MBOX_TAG_GETSERIAL;
    mbox[3] = 8;
    mbox[4] = 8;
    mbox[5] = 0;
    mbox[6] = 0;

    mbox[7] = MBOX_TAG_LAST;

    if (mbox_call(MBOX_CH_PROP))
    {
        unsigned long long serial = (unsigned long long)mbox[6] << 32;
        serial |= mbox[5];
        return serial;
    }

    return NULL;
}
