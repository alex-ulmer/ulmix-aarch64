#ifndef UART_H
#define UART_H

void setup_uart0();
void uart_putc(unsigned char c);

#endif
