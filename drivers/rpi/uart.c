#include <mbox.h>
#include <types.h>
#include <gpio.h>
#include <asm.h>
#include "uart.h"

#define UART0_DR        ((volatile unsigned int*)(MMIO_BASE+0x00201000))
#define UART0_FR        ((volatile unsigned int*)(MMIO_BASE+0x00201018))
#define UART0_IBRD      ((volatile unsigned int*)(MMIO_BASE+0x00201024))
#define UART0_FBRD      ((volatile unsigned int*)(MMIO_BASE+0x00201028))
#define UART0_LCRH      ((volatile unsigned int*)(MMIO_BASE+0x0020102C))
#define UART0_CR        ((volatile unsigned int*)(MMIO_BASE+0x00201030))
#define UART0_IMSC      ((volatile unsigned int*)(MMIO_BASE+0x00201038))
#define UART0_ICR       ((volatile unsigned int*)(MMIO_BASE+0x00201044))

void setup_uart0()
{
    // UART0 off
    *UART0_CR = 0;

    // setup clock (4MHz)
    mbox[0] = 9*4;
    mbox[1] = MBOX_REQUEST;
    mbox[2] = MBOX_TAG_SETCLKRATE;
    mbox[3] = 12;
    mbox[4] = 8;
    mbox[5] = 2;
    mbox[6] = 4000000;
    mbox[7] = 0;
    mbox[8] = MBOX_TAG_LAST;
    mbox_call(MBOX_CH_PROP);

    // map GPIO pins 14,15 to UART0 (alt0)
    gpio_pinmode(GPIO_UART0_TX, GPIO_ALT0, GPIO_NOPULL);
    gpio_pinmode(GPIO_UART0_RX, GPIO_ALT0, GPIO_NOPULL);

    // configure UART0
    *UART0_ICR = 0x7FF;     // clear interrupts
    *UART0_IBRD = 2;        // 115200 baud
    *UART0_FBRD = 0xB;
    *UART0_LCRH = 0b11<<5;  // 8n1
    *UART0_CR = 0x301;      // enable Tx, Rx, FIFO
}

void uart_putc(unsigned char c)
{
    while (*UART0_FR & (1 << 5));
    *UART0_DR = c;
}

unsigned char uart_getrc()
{
    do { __asm__ volatile ("nop"); }
    while (*UART0_FR & 0x10);
    return (unsigned char)(*UART0_DR);
}
