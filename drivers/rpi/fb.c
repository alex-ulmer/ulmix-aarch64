#include <mbox.h>

#include <debug.h>

static unsigned int width, height, pitch;
static unsigned char *framebuffer;

void fb_setup(unsigned int vwidth, unsigned int vheight)
{
    mbox[0] = 35*4;
    mbox[1] = MBOX_REQUEST;

    // physical screen width, height
    mbox[2] = 0x48003;
    mbox[3] = 8;
    mbox[4] = 8;
    mbox[5] = vwidth;
    mbox[6] = vheight;

    // virtual screen width, height
    mbox[7] = 0x48004;
    mbox[8] = 8;
    mbox[9] = 8;
    mbox[10] = vwidth;
    mbox[11] = vheight;

    // virtual offset
    mbox[12] = 0x48009;
    mbox[13] = 8;
    mbox[14] = 8;
    mbox[15] = 0;           //FrameBufferInfo.x_offset
    mbox[16] = 0;           //FrameBufferInfo.y.offset

    mbox[17] = 0x48005; //set depth
    mbox[18] = 4;
    mbox[19] = 4;
    mbox[20] = 32;          //FrameBufferInfo.depth

    mbox[21] = 0x48006; //set pixel order
    mbox[22] = 4;
    mbox[23] = 4;
    mbox[24] = 1;           //RGB, not BGR preferably

    mbox[25] = 0x40001; //get framebuffer, gets alignment on request
    mbox[26] = 8;
    mbox[27] = 8;
    mbox[28] = 4096;        //FrameBufferInfo.pointer
    mbox[29] = 0;           //FrameBufferInfo.size

    mbox[30] = 0x40008; //get pitch
    mbox[31] = 4;
    mbox[32] = 4;
    mbox[33] = 0;           //FrameBufferInfo.pitch

    mbox[34] = MBOX_TAG_LAST;

    if (mbox_call(MBOX_CH_PROP) && mbox[20] == 32 && mbox[28] != 0)
    {
        mbox[28] &= 0x3fffffff;
        width = mbox[5];
        height = mbox[6];
        pitch = mbox[33];

        framebuffer = (void*)((unsigned long)mbox[28]);
        return;
    }

    kprintf(KDEBUG, "fb: error: cannot set resolution %dx%d\n",
            width, height);
}

void fb_splash()
{
    unsigned int x, y;
    unsigned int pxl;
    unsigned int *fbptr = (void*)framebuffer;

    for (y = 0; y < height; y++)
    {
        if (y < height / 6)
            pxl = 0x00ff0000;
        else if (y >= (height / 6) && y < (height / 6)*2)
            pxl = 0x0000ff00;
        else if (y >= (height / 6)*2 && y < (height / 6)*3)
            pxl = 0x0000ffff;
        else if (y >= (height / 6)*3 && y < (height / 6)*4)
            pxl = 0x00ff00ff;
        else if (y >= (height / 6)*4 && y < (height / 6)*5)
            pxl = 0x0000ffff;
        else if (y >= (height / 6)*5 && y < height)
            pxl = 0x00ffff00;

        for (x = 0; x < width; x++)
        {
            *fbptr = pxl;
            fbptr++;
        }

        // ptr += pitch - homer_width
    }
}
