#include <debug.h>
#include <types.h>
#include <string.h>

// TEMPORARY:
extern void uart_init();
extern void uart_putc(char c);

#define PF_LONG (1 << 0)
#define PF_PAD  (1 << 1)

static inline void kputc(char c)
{
    uart_putc(c);
}

static void kputs(char *s)
{
    while (*s)
        kputc(*s++);
}

static void do_kprintf(const char *fmt, va_list ap)
{
    int flags = 0;
    char *arg_str;
    char sbuf[64];
    unsigned long arg_int;
    int padding;

    int len = strlen(fmt);
    for (int i = 0; i < len; i++) {
        padding = 1;
        if (fmt[i] == '%') {

            // check for flags
            switch (fmt[++i]) {
            case 'l':
                flags |= PF_LONG;
                break;
            case '0':
                flags |= PF_PAD;
                break;
            default:
                i--;
            }

            // switch between data type
            switch(fmt[++i]) {
            case 'x':
                if (flags & PF_LONG) {
                    arg_int = va_arg(ap, unsigned long);
                } else {
                    arg_int = (unsigned long)va_arg(ap, unsigned int);
                }
                kputs(xtoa(arg_int, sbuf, padding));
                break;

            case 'S':
            case 'd':

            case 'p':
                arg_int = va_arg(ap, unsigned long);
                kputs("0x");
                kputs(xtoa(arg_int, sbuf, sizeof(void*)));
                break;

            case 's':
                arg_str = va_arg(ap, char*);
                if (arg_str)
                    kputs(arg_str);
                break;

            case '%':
                kputc('%');
                break;
            }

            continue;
        }

        kputc(fmt[i]);
    }

    va_end(ap);
}

void kprintf(int loglevel, const char *fmt, ...)
{
    (void)loglevel;

    va_list args;
    va_start(args, fmt);
    do_kprintf(fmt, args);
    va_end(args);
}
