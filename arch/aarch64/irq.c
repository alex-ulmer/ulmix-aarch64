#include <debug.h>
#include <asm.h>

void irq_handler(unsigned long irq)
{

    kprintf(KDEBUG, "irq() at EL%x\n", get_el());
}

void syserror()
{
    kprintf(KDEBUG, "syserror() at EL%x\n", get_el());
}

void syserror_sync()
{
    unsigned int error;
    unsigned long addr;
    __asm__ ("mrs %0, esr_el1;"
            "mrs %1, elr_el1"
            : "=r"(error), "=r"(addr));
    kprintf(KDEBUG, "syserror_sync() at EL%x, error=%p, addr=%p\n", get_el(), error, addr);
    while (1);
}
