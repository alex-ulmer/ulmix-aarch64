TARGET_ARCH		= aarch64
TARGET_BOARD	= rpi3
DEBUG 			= on
DEFINES			= -D _BOARD_="$(TARGET_BOARD)"

CC		= aarch64-linux-gnu-gcc
AS		= $(CC)
LD		= aarch64-linux-gnu-ld
AR		= aarch64-linux-gnu-ar crs
OBJCOPY = aarch64-linux-gnu-objcopy

ifeq ($(DEBUG),on)
	DBFLAGS	= -g -O0 -D _DEBUG_
else
	DBFLAGS = -s
endif

CCFLAGS	= -c -std=c11 -Wshadow -Wall $(CINCL) $(DEFINES) \
		  -ffreestanding -nostdinc -fno-strict-aliasing -fno-builtin \
		  -fno-stack-protector -fno-omit-frame-pointer -fno-common \
		  -fno-pic -fno-delete-null-pointer-checks $(DBFLAGS)
LDFLAGS	= -T $(LDINFO) -nostdlib -nmagic -gc-sections --warn-common
ASFLAGS	= $(CCFLAGS)

%.o: %.c
	@ echo " CC  $<"
	@ $(CC) $< $(CCFLAGS) -o $@

%.o: %.s
	@ echo " AS  $<"
	@ $(AS) $< $(ASFLAGS) -o $@
