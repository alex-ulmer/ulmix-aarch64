#ifndef SDCARD_H
#define SDCARD_H

#define SDCARD_SUCCESS  0
#define SDCARD_TIMEOUT  -1
#define SDCARD_ERROR    -2

int setup_sdcard();

#endif // SDCARD_H
