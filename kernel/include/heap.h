#ifndef HEAP_H
#define HEAP_H

/*
 * heap implementation was taken unchanged from ulmix-i386
 * Copyright (C) 2018-2019
 * Written by Alexander Ulmer <alexulmer1999@gmail.com>
 */

struct kheape_struct
{
    unsigned int            available;      // 1 = unallocated, 0 = allocated
    unsigned long           size;           // actual payload size (without header)
    void *                  start;          // start address of payload memory
    struct kheape_struct *  next;           // next kheap_entry_t
    struct kheape_struct *  previous;       // previous kheap_entry_t
    const char *            description;    // description for debugging purposes
} __attribute__((packed));

void setup_heap(void *start_addr, unsigned long max_size);
void* kmalloc(unsigned long size, unsigned align, const char *description);
void kfree(void *ptr);

void heap_dump(void);

void *kmalloc(unsigned long size, unsigned align, const char *description);
void kfree(void *mptr);

#endif // HEAP_H
