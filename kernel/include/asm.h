#ifndef ASM_H
#define ASM_H

#define BIT(x) (1 << x)

#define __init

void wait_cycles(unsigned long n);
void wait_msec(unsigned long n);

unsigned get_el();

#endif // ASM_H
