#include <asm.h>
#include <debug.h>
#include <string.h>
#include <board.h>
#include <heap.h>
#include <types.h>

extern void *__start;
extern void *__bss_start;
extern void *__bss_size;
extern void *__end;

extern unsigned xlevel;

void _main()
{
    // clear BSS
    bzero(&__bss_start, (size_t)&__bss_size);

    kprintf(KDEBUG, "kernel @ 0x%x - 0x%x (BSS 0x%x, size 0x%x)\n"
                    "running at EL%x\n",
            &__start, &__end, &__bss_start, &__bss_size,
            get_el());

    setup_heap(0x0, 0x80000);

    iosetup();

    heap_dump();
    panic();
}
