#include <asm.h>

void panic()
{
    for (;;)
    {
        __asm__ volatile ("wfe");
    }
}

unsigned get_el()
{
    unsigned el;
    __asm__ ("mrs %0, CurrentEL" : "=r"(el));
    return el >> 2;
}

void wait_cycles(unsigned long n)
{
    while (n--)
    {
        __asm__ volatile ("nop");
    }
}

void wait_msec(unsigned long n)
{
    register unsigned long f, t, r;

    __asm__ volatile ("mrs %0, cntfrq_el0" : "=r"(f));
    __asm__ volatile ("mrs %0, cntpct_el0" : "=r"(t));

    t += ((f / 1000) * n) / 1000;

    do
    {
        __asm__ volatile ("mrs %0, cntpct_el0" : "=r"(r));
    }
    while (r < t);
}
