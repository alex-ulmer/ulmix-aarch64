#ifndef DEVICENUM_H
#define DEVICENUM_H

/* block devices */
#define MAJOR_MMC0  1
#define MAJOR_MMC1  2
#define MAJOR_MMC2  3
#define MAJOR_MMC3  4

/* character devices */
#define MAJOR_GPIO      0
#define MAJOR_KEYBOARD  1
#define MAJOR_MOUSE     2
#define MAJOR_UART0     3
#define MAJOR_UART1     4


#endif // DEVICENUM_H
