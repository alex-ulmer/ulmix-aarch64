/*
 * Copyright (C) 2018 bzt (bztsrc@github)
 * SD EMMC Host Controller on the Raspberry Pi
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */


#include <gpio.h>
#include <asm.h>
#include <types.h>
#include <debug.h>
#include <devices.h>
#include <mem.h>
#include <errno.h>

#include "sdcard.h"

/* GPIO pins going to the SD card slot */
#define SDCARD_CD       47
#define SDCARD_CLK      48
#define SDCARD_CMD      49
#define SDCARD_DATA0    50
#define SDCARD_DATA1    51
#define SDCARD_DATA2    52
#define SDCARD_DATA3    53

/* SD Host Controller Registers */
#define EMMC_ARG2           MMIO(0x00300000)    // argument 2 or SDMA system address
#define EMMC_BLKSIZECNT     MMIO(0x00300004)    // block size and block count
#define EMMC_ARG1           MMIO(0x00300008)    // argument 1
#define EMMC_CMDTM          MMIO(0x0030000C)    // transfer mode and command
#define EMMC_RESP0          MMIO(0x00300010)    // response 0, 1
#define EMMC_RESP1          MMIO(0x00300014)    // response 2, 3
#define EMMC_RESP2          MMIO(0x00300018)    // response 4, 5
#define EMMC_RESP3          MMIO(0x0030001C)    // response 6, 7
#define EMMC_DATA           MMIO(0x00300020)    // data buffer port
#define EMMC_STATUS         MMIO(0x00300024)    // status
#define EMMC_CONTROL0       MMIO(0x00300028)    // host ctrl1, power ctrl, block gap ctrl, wakeup ctrl
#define EMMC_CONTROL1       MMIO(0x0030002C)    // clock ctrl, timeout ctrl, software reset
#define EMMC_INTERRUPT      MMIO(0x00300030)    // normal interrupt, error interrupt status
#define EMMC_INT_MASK       MMIO(0x00300034)    // normal interrupt, error interrupt status enable
#define EMMC_INT_EN         MMIO(0x00300038)    // normal interrupt, error interrupt signal enable
#define EMMC_CONTROL2       MMIO(0x0030003C)    // CMD error, host ctrl 2
#define EMMC_CAP0           MMIO(0x00300040)    // capabilities
#define EMMC_CAP1           MMIO(0x00300044)    // capabilities
#define EMMC_SLOTISR_VER    MMIO(0x003000FC)    // host controller spec, vendor flags

/* IRQ flags */
#define INT_DATA_TIMEOUT    0x00100000
#define INT_CMD_TIMEOUT     0x00010000
#define INT_READ_RDY        0x00000020
#define INT_CMD_DONE        0x00000001
#define INT_ERROR_MASK      0x017E8000

/* SD Host Controller commands */
#define CMD_NEED_APP        0x80000000
#define CMD_RSPNS_48        0x00020000
#define CMD_ERRORS_MASK     0xfff9c004
#define CMD_RCA_MASK        0xffff0000

#define CMD_GO_IDLE         0x00000000
#define CMD_SEND_IF_COND    0x08020000
#define CMD_ALL_SEND_CID    0x02010000
#define CMD_SEND_REL_ADDR   0x03020000
#define CMD_CARD_SELECT     0x07030000
#define CMD_STOP_TRANS      0x0C030000
#define CMD_READ_SINGLE     0x11220010
#define CMD_READ_MULTI      0x12220032
#define CMD_SET_BLOCKCNT    0x17020000
#define CMD_APP_CMD         0x37000000
#define CMD_SET_BUS_WIDTH   (0x06020000 | CMD_NEED_APP)
#define CMD_SEND_OP_COND    (0x29020000 | CMD_NEED_APP)
#define CMD_SEND_SCR        (0x33220010 | CMD_NEED_APP)

/* flags for EMMC_CONTROL0 */
#define C0_SPI_MODE_EN      0x00100000
#define C0_HCTL_HS_EN       0x00000004
#define C0_HCTL_DWITDH      0x00000002

/* flags for EMMC_CONTROL1 */
#define CTRL1_CLK_INTLEN    BIT(0)
#define CTRL1_CLK_STABLE    BIT(1)
#define CTRL1_CLK_EN        BIT(2)
#define CTRL1_RSET_HOST     BIT(24)
#define CTRL1_RSET_CMD      BIT(25)
#define CTRL1_RSET_DATA     BIT(26)

#define C1_TOUNIT_MAX       0x000e0000

/* flags for EMMC_STATUS */
#define STAT_READ_AVAILABLE 0x00000800
#define STAT_DAT_INHIBIT    0x00000002
#define STAT_CMD_INHIBIT    0x00000001
#define STAT_APP_CMD        0x00000020

#define ACMD41_VOLTAGE      0x00ff8000
#define ACMD41_CMD_COMPLETE 0x80000000
#define ACMD41_CMD_CCS      0x40000000
#define ACMD41_ARG_HC       0x51ff8000

#define SCR_SD_BUS_WIDTH_4  0x00000400
#define SCR_SUPP_SET_BLKCNT 0x02000000
#define SCR_SUPP_CCS        0x00000001

/* SD Host Controller spec version */
#define SPECV3  2
#define SPECV2  1
#define SPECV1  0

#define BLOCKSIZE 512

struct mmc_struct
{
    unsigned spec_version;
    unsigned vendor;
};

static unsigned long cmd_err = 0;
static unsigned long sd_rca = 0;
static unsigned char sd_spec_version;
static unsigned long sd_scr[2];

static int reset_emmc()
{
    kprintf(KDEBUG, "emmc: reset\n");
    *EMMC_CONTROL0 = 0;
    *EMMC_CONTROL1 |= CTRL1_RSET_HOST;

    int timeout = 10000;
    do wait_msec(10);
    while ((*EMMC_CONTROL1 & CTRL1_RSET_HOST) && timeout--);

    if (timeout <= 0)
    {
        kprintf(KERROR, "emmc: error: reset timed out\n");
        return SDCARD_TIMEOUT;
    }

    return SDCARD_SUCCESS;
}

static int set_clk(unsigned long freq)
{
    unsigned int d, x, s = 32, h = 0, c = 41666666 / freq;

    int timeout = 100000;
    while ((*EMMC_STATUS & (STAT_CMD_INHIBIT | STAT_DAT_INHIBIT)) && timeout--)
        wait_msec(1);
    if (timeout <= 0)
    {
        kprintf(KERROR, "emmc: error: timeout setting clock freq\n");
        return SDCARD_TIMEOUT;
    }

    *EMMC_CONTROL1 &= ~CTRL1_CLK_EN;
    wait_msec(10);

    x = c - 1;
    if (!x)
    {
        s = 0;
    }
    else
    {
        if (!(x & 0xffff0000u)) { x <<= 16; s -= 16; }
        if (!(x & 0xff000000u)) { x <<= 8;  s -= 8;  }
        if (!(x & 0xf0000000u)) { x <<= 4;  s -= 4;  }
        if (!(x & 0xc0000000u)) { x <<= 2;  s -= 2;  }
        if (!(x & 0x80000000u)) { x <<= 1;  s -= 1;  }
        if (s > 0) s--;
        if (s > 7) s = 7;
    }

    if (sd_spec_version > SPECV2)
        d = c;
    else
        d = (1 << s);
    d = ((d & 0xff) << 8) | h;

    *EMMC_CONTROL1 = (*EMMC_CONTROL1 & 0xffff003f) | d;
    wait_msec(10);
    *EMMC_CONTROL1 |= CTRL1_CLK_EN;
    wait_msec(10);

    timeout = 10000;
    while (!(*EMMC_CONTROL1 & CTRL1_CLK_STABLE) && timeout--)
        wait_msec(10);
    if (timeout <= 0)
    {
        kprintf(KERROR, "emmc: error: no stable clock\n");
        return SDCARD_TIMEOUT;
    }

    return SDCARD_SUCCESS;
}

static int sd_status(unsigned int mask)
{
    int timeout = 500000;
    while((*EMMC_STATUS & mask) && !(*EMMC_INTERRUPT & INT_ERROR_MASK) && timeout--)
        wait_msec(1);
    return (timeout <= 0 || (*EMMC_INTERRUPT & INT_ERROR_MASK)) ? SDCARD_ERROR : SDCARD_SUCCESS;
}

static int sd_int(unsigned int mask)
{
    unsigned int r, m = mask | INT_ERROR_MASK;

    int timeout = 1000000;
    while(!(*EMMC_INTERRUPT & m) && timeout--)
        wait_msec(1);

    r = *EMMC_INTERRUPT;
    if (timeout <= 0 || (r & INT_CMD_TIMEOUT) || (r & INT_DATA_TIMEOUT))
    {
        *EMMC_INTERRUPT = r;
        return SDCARD_TIMEOUT;
    }
    else if (r & INT_ERROR_MASK)
    {
        *EMMC_INTERRUPT=r;
        return SDCARD_ERROR;
    }

    *EMMC_INTERRUPT = mask;
    return SDCARD_SUCCESS;
}

static int cmd(unsigned code, unsigned arg)
{
    int r = cmd_err = 0;

    if (code & CMD_NEED_APP)
    {
        r = cmd(CMD_APP_CMD | (sd_rca ? CMD_RSPNS_48 : 0), sd_rca);
        if (sd_rca && !r)
        {
            kprintf(KERROR, "emmc: error: command SD_APP failed\n");
            return cmd_err = SDCARD_ERROR;
            code &= ~CMD_NEED_APP;
        }
    }

    if (sd_status(STAT_CMD_INHIBIT))
    {
        kprintf(KERROR, "emmc: error: is busy\n");
        return cmd_err = SDCARD_TIMEOUT;
    }

    //kprintf(KDEBUG, "emmc: cmd=0x%x, arg=0x%x\n", code, arg);
    *EMMC_INTERRUPT = *EMMC_INTERRUPT;
    *EMMC_ARG1 = arg;
    *EMMC_CMDTM = code;

    if (code == CMD_SEND_OP_COND)
        wait_msec(1000);
    else if (code == CMD_SEND_IF_COND || code == CMD_APP_CMD)
        wait_msec(100);

    if ((r = sd_int(INT_CMD_DONE)) < 0)
    {
        kprintf(KERROR, "emmc: error: sending command failed\n");
        return cmd_err = r;
    }

    r = *EMMC_RESP0;

    if (code == CMD_GO_IDLE || code == CMD_APP_CMD)
        return 0;
    else if (code == (CMD_APP_CMD | CMD_RSPNS_48))
        return r & STAT_APP_CMD;
    else if (code == CMD_SEND_OP_COND)
        return r;
    else if (code == CMD_SEND_IF_COND)
        return (r == arg) ? SDCARD_SUCCESS : SDCARD_ERROR;
    else if (code == CMD_ALL_SEND_CID)
    {
        r|= *EMMC_RESP3;
        r |= *EMMC_RESP2;
        r |= *EMMC_RESP1;
        return r;
    }
    else if (code == CMD_SEND_REL_ADDR)
    {
        cmd_err = (((r & 0x1fff))
                  | ((r & 0x2000) << 6)
                  | ((r & 0x4000) << 8)
                  | ((r & 0x8000) << 8)) & CMD_ERRORS_MASK;
        return r & CMD_RCA_MASK;
    }

    return r & CMD_ERRORS_MASK;
}

static ssize_t sdcard_read(char *buffer, size_t count, size_t offset)
{
    if (count == 0)
        return 0;

    if (sd_status(STAT_DAT_INHIBIT))
        return -EIO;

    unsigned int *buffer4 = (unsigned int *)buffer;
    if (sd_scr[0] & SCR_SUPP_CCS)
    {
        if (count > 1 && (sd_scr[0] & SCR_SUPP_SET_BLKCNT))
        {
            cmd(CMD_SET_BLOCKCNT, count);
            if (cmd_err) return -EIO;
        }
        *EMMC_BLKSIZECNT = (count << 16) | 512;
        cmd((count == 1) ? CMD_READ_SINGLE : CMD_READ_MULTI, offset);
        if (cmd_err) return -EIO;
    }
    else
    {
        *EMMC_BLKSIZECNT = (1 << 16) | 512;
    }

    unsigned r, d, c = 0;
    while (c < count)
    {
        if (!(sd_scr[0] & SCR_SUPP_CCS))
        {
            cmd(CMD_READ_SINGLE, (offset + c) * 512);
            if (cmd_err) return -EIO;
        }

        if ((r = sd_int(INT_READ_RDY)))
            return -EIO;

        for (d = 0; d < 128; d++)
            buffer4[d] = *EMMC_DATA;
        c++; buffer4 += 128;
    }

    if (count > 1 && !(sd_scr[0] & SCR_SUPP_SET_BLKCNT) &&
            (sd_scr[0] & SCR_SUPP_CCS))
        cmd(CMD_STOP_TRANS, 0);
    if (cmd_err) return -EIO;

    return count;
}

static ssize_t sdcard_write(char *buffer, size_t count, size_t offset)
{
    return -ENOSYS;
}

int setup_sdcard()
{

    // setup GPIO control pins
    gpio_pinmode(SDCARD_CD, GPIO_INPUT, GPIO_PULLUP);
    gpio_trigger_enable(SDCARD_CD, GPIO_LVL_HIGH);

    gpio_pinmode(SDCARD_CLK, GPIO_ALT3, GPIO_PULLUP);
    gpio_pinmode(SDCARD_CMD, GPIO_ALT3, GPIO_PULLUP);

    gpio_pinmode(SDCARD_DATA0, GPIO_ALT3, GPIO_PULLUP);
    gpio_pinmode(SDCARD_DATA1, GPIO_ALT3, GPIO_PULLUP);
    gpio_pinmode(SDCARD_DATA2, GPIO_ALT3, GPIO_PULLUP);
    gpio_pinmode(SDCARD_DATA3, GPIO_ALT3, GPIO_PULLUP);


    /* get version information */
    struct mmc_struct mmc;
    sd_spec_version = mmc.spec_version = (*EMMC_SLOTISR_VER >> 16) & 0xff;
    mmc.vendor = *EMMC_SLOTISR_VER >> 24;

    if (mmc.spec_version < SPECV3)
    {
        kprintf(KERROR, "emmc: unsupported host controller (version %d)\n");
        return -EOPNOTSUPP;
    }

    // reset
    long error;
    if ((error = reset_emmc()) < 0)
        return error;

    long ccs, r;
    *EMMC_CONTROL1 |= CTRL1_CLK_INTLEN | C1_TOUNIT_MAX;
    wait_msec(10);

    // setup clock frequency
    if ((error = set_clk(400000)) < 0)
        return error;

    // setup interrupt masks
    *EMMC_INT_EN    = 0xffffffff;
    *EMMC_INT_MASK  = 0xffffffff;

    cmd(CMD_GO_IDLE, 0);
    if (cmd_err) return cmd_err;

    cmd(CMD_SEND_IF_COND, 0x000001aa);
    if (cmd_err) return cmd_err;

    int timeout = 6;
    while (!(r & ACMD41_CMD_COMPLETE) && timeout--)
    {
        wait_cycles(400);

        r = cmd(CMD_SEND_OP_COND, ACMD41_ARG_HC);
        if (cmd_err) return cmd_err;

        kprintf(KDEBUG, "emmc: flags: ");
        if (r & ACMD41_CMD_COMPLETE)
            kprintf(KDEBUG, "complete ");
        if (r & ACMD41_VOLTAGE)
            kprintf(KDEBUG, "voltage ");
        if (r & ACMD41_CMD_CCS)
            kprintf(KDEBUG, "ccs");
        kprintf(KDEBUG, "\n");
    }

    if (!(r & ACMD41_CMD_COMPLETE) || timeout <= 0)
        return SDCARD_TIMEOUT;
    if (!(r & ACMD41_VOLTAGE))
        return SDCARD_ERROR;
    if (r & ACMD41_CMD_CCS)
        ccs = SCR_SUPP_CCS;

    cmd(CMD_ALL_SEND_CID, 0);
    if (cmd_err) return cmd_err;

    sd_rca = cmd(CMD_SEND_REL_ADDR,0);
    if (cmd_err) return cmd_err;

    // set clock to 25 MHz
    if ((error = set_clk(25000000)) < 0)
        return error;

    cmd(CMD_CARD_SELECT, sd_rca);
    if (cmd_err) return cmd_err;

    if (sd_status(STAT_DAT_INHIBIT))
        return SDCARD_TIMEOUT;

    *EMMC_BLKSIZECNT = (1 << 16) | 8;
    cmd(CMD_SEND_SCR, 0);
    if (cmd_err) return cmd_err;
    if (sd_int(INT_READ_RDY))
        return SDCARD_TIMEOUT;

    r = 0;
    timeout = 100000;
    while (r < 2 && timeout--)
    {
        if (*EMMC_STATUS & STAT_READ_AVAILABLE)
            sd_scr[r++] = *EMMC_DATA;
        else
            wait_msec(1);
    }

    if (r != 2)
        return SDCARD_TIMEOUT;

    if (sd_scr[0] & SCR_SD_BUS_WIDTH_4)
    {
        cmd(CMD_SET_BUS_WIDTH, sd_rca | 2);
        if (cmd_err) return cmd_err;
        *EMMC_CONTROL0 |= C0_HCTL_DWITDH;
    }

    sd_scr[0] &= ~SCR_SUPP_CCS;
    sd_scr[0] |= ccs;

    kprintf(KDEBUG, "emmc: initialized SD Host Controller\n");

    // add to the system's device tree
    struct gendisk_struct *sdcard = kmalloc(sizeof(struct gendisk_struct),
                                            1, "gendisk_struct sdcard");
    sdcard->major = MAJOR_MMC0;
    sdcard->minor = 0;
    sdcard->capacity = (size_t)-1;  // not yet known
    sdcard->io_size = 512;          // block size

    sdcard->fops.read = sdcard_read;
    sdcard->fops.write = sdcard_write;

    register_blkdev(sdcard);
    return SDCARD_SUCCESS;
}
