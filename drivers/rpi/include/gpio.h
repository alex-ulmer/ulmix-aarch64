#ifndef GPIO_H
#define GPIO_H

#define GPIO_INPUT      0b000
#define GPIO_OUTPUT     0b001
#define GPIO_ALT0       0b100
#define GPIO_ALT1       0b101
#define GPIO_ALT2       0b110
#define GPIO_ALT3       0b111
#define GPIO_ALT4       0b011
#define GPIO_ALT5       0b010

#define GPIO_NOPULL     0
#define GPIO_PULLDOWN   1
#define GPIO_PULLUP     2

#define GPIO_UART0_TX   14
#define GPIO_UART0_RX   15
#define GPIO_ACTLED     29  // RPi 3B+ only

#define GPIO_LVL_HIGH   1
#define GPIO_LVL_LOW    2
#define GPIO_EDGE_RISE  4
#define GPIO_EDGE_FALL  8

// GPIO configuration functions
void gpio_pinmode(int pin, unsigned pinmode, unsigned rpull);
void gpio_trigger_enable(int pin, unsigned trigmode);
void gpio_setpull(int pin, unsigned rpull);
void gpio_set(int pin, int val);

// turn ACT LED on() or off()
static inline void act()    { gpio_set(GPIO_ACTLED, 1); }
static inline void noact()  { gpio_set(GPIO_ACTLED, 0); }
void setup_gpio(void);

#endif // GPIO_H
