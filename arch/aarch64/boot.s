.section ".text.boot"
.global _start
.global _cores

_start:
    // get core id
    mrs x1, mpidr_el1
    and x1, x1, #3

    // halt if core id != 0
    cbz x1, .sboot

.coreloop:
    wfe
    b .coreloop

.sboot:
    // directly jump to main if we are EL < 2
    mrs x2, currentel
    lsr x2, x2, #2
    sub x2, x2, #1
    cbz x2, _main

    // install exception vector table
    ldr x1, =ivt
    msr vbar_el1, x1
    msr vbar_el2, x1

    // drop down to EL1
    // execute EL1 in AArch64 mode
    mov x1, #(1 << 31)
    msr hcr_el2, x1

    // enable instruction cache
    mov x1, #0x800
    msr sctlr_el1, x1

    // supervisor trap settings
    mov x1, #(3 << 20)
    msr cptr_el2, x1
    msr hstr_el2, xzr

    // enable floating point functionality
    mov x1, #(3 << 20)
    msr cpacr_el1, x1

    // mask interrupts
    mov x1, #0x3c5
    msr spsr_el2, x1

    // setup a stack for EL1
    mov x1, #0x80000
    msr sp_el1, x1

    // exception return addr = _main
    ldr x1, =_main
    msr elr_el2, x1
    eret

_cores:
    .long 0x0


.balign 0x800
ivt:

// Vector for current EL, SP0
sync_elc_sp0:
    bl syserror_sync
    eret
.balign 0x80
irq_elc_sp0:
    bl irq_handler
    eret
.balign 0x80
fiq_elc_sp0:
    bl irq_handler
    eret
.balign 0x80
serror_elc_sp0:
    bl syserror
    eret

// current EL, SPx
.balign 0x80
sync_elc_spx:
    bl syserror_sync
    eret
.balign 0x80
irq_elc_spx:
    bl irq_handler
    eret
.balign 0x80
fiq_elc_spx:
    bl irq_handler
    eret
.balign 0x80
serror_elc_spx:
    bl syserror
    eret

// lower EL, from AArch64
.balign 0x80
sync_ell_a64:
    bl syserror_sync
    eret
.balign 0x80
irq_ell_a64:
    bl irq_handler
    eret
.balign 0x80
fiq_ell_a64:
    bl irq_handler
    eret
.balign 0x80
serror_ell_a64:
    bl syserror
    eret

// lower EL, from AArch32
.balign 0x80
sync_ell_a32:
    bl syserror_sync
    eret
.balign 0x80
irq_ell_a32:
    bl irq_handler
    eret
.balign 0x80
fiq_ell_a32:
    bl irq_handler
    eret
.balign 0x80
serror_ell_a32:
    bl syserror
    eret
