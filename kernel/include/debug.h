#ifndef DEBUG_H
#define DEBUG_H

#define KDEBUG  0
#define KINFO   1
#define KERROR  2
#define KFATAL  3

void kprintf(int loglevel, const char *fmt, ...);

void panic();

#endif // DEBUG_H
